#include <iostream>
template <typename Iter>
void insert_sort(Iter begin, Iter end, std::random_access_iterator_tag) {
    Iter save;
    Iter stop = begin;
    while (stop <= end) {
        save = stop;
        while ( stop != begin && *(stop-1) > *stop) {
            std::swap(*(stop - 1), *stop);
            stop--;
        }
        stop = save;
        stop++;
    }
}

template <typename Iter>
void insert_sort(Iter begin, Iter end) {
    typedef typename std::iterator_traits<Iter>::iterator_category category;
    return insert_sort(begin, end, category());
}
int main() {
    int i[3] = {3,2,1};
    insert_sort(i, i+2);
    for (int k : i) {
        std::cout << k << std::endl;
    }
    return 0;
}
