#include <iostream>
#include <vector>

template <typename Iter>
void select_sort(Iter begin, Iter end, std::random_access_iterator_tag) {
    Iter exchange;
    while (begin != end) {
        exchange = begin;
        while (exchange != end) {
            exchange++;
            if (*exchange < *begin) {
                std::swap(*begin, *exchange);
            }
        }
        begin++;
    }
}

template <typename Iter>
void select_sort(Iter begin, Iter end) {
    typedef typename std::iterator_traits<Iter>::iterator_category category;
    return select_sort(begin, end, category());
}

int main() {
    //test vector
    std::vector<int> a = {3,2,1};
    select_sort(a.begin(), a.end() - 1);
    for (int i : a) {
        std::cout << i << std::endl;
    }

    //test array
    int i[3] = {3,2,1};
    select_sort(i, i+2);
    for (int k : i) {
        std::cout << k << std::endl;
    }
    return 0;
}